public class RefactoredSingleOperatorReversePolishNotation {

	public static void main(String[] args) {
		double result = Double.valueOf(args[0]);
		for (int i = 1; i < args.length - 1; i++) {
			switch (getOperator(args)) {
			case "+":
				result += Double.valueOf(args[i]);
				break;
			case "-":
				result -= Double.valueOf(args[i]);
				break;
			case "/":
				double value = Double.valueOf(args[i]);
				if (value != 0) {
					result /= value;
				}
				break;
			case "*":
				result *= Double.valueOf(args[i]);
				break;
			}
		}

		System.out.println("The result is " + result);
	}
	
	private static String getOperator(String[] args) {
		return args[args.length - 1];
	}
	
	
}
