public class SingleOperatorReversePolishNotation {

	public static void main(String[] args) {
		int lastIndex = args.length - 1;
		final String operator = args[lastIndex];

		double result = Double.valueOf(args[0]);
		for (int i = 1; i < lastIndex; i++) {
			switch (operator) {
			case "+":
				result += Double.valueOf(args[i]);
				break;
			case "-":
				result -= Double.valueOf(args[i]);
				break;
			case "/":
				double value = Double.valueOf(args[i]);
				if (value != 0) {
					result /= value;
				}
				break;
			case "*":
				result *= Double.valueOf(args[i]);
				break;
			}
		}

		System.out.println("The result is " + result);
	}

}
