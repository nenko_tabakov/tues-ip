
public class DivideTwoIntegers {

	public static void main(String[] args) {
		int a = 3;
		int b = 5;
		System.out.print("The result of int division is : ");
		System.out.println(a / b);
		
		System.out.print("The result of int division casted to float is : ");
		System.out.println((float)(a / b));
		
		System.out.print("The result of int division with dominator casted to float is : ");
		System.out.println(a / (float)b);
	}
}
