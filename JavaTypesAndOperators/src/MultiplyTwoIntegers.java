
public class MultiplyTwoIntegers {

	public static void main(String[] args) {
		int a = 3;
		int b = 5;
		System.out.print("The result of int multiplication is : ");
		System.out.println(a * b);
		
		float c = 4.2f;
		
		System.out.print("The result of int multiplied with float is : ");
		System.out.println((a * c));
		
		System.out.print("The result of int multiplied with float casted to int is : ");
		System.out.println((int)(a * c));
	}
}
