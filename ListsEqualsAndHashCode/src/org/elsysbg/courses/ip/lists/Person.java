package org.elsysbg.courses.ip.lists;
public class Person {

	private String firstname;
	private String surname;
	private String familyname;

	public Person(String firstname, String surname, String familyname) {
		this.firstname = firstname;
		this.surname = surname;
		this.familyname = familyname;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getFamilyname() {
		return familyname;
	}

	public void setFamilyname(String familyname) {
		this.familyname = familyname;
	}

}
