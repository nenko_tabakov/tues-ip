package org.elsysbg.courses.ip.lists;

import java.util.ArrayList;

public class ListWithPersons {

	public static void main(String[] args) {
		ArrayList<Person> persons = new ArrayList<Person>();

		Person personOfInterest = new Person("first", "second", "last");

		persons.add(new Person("erwrw1", "rewrw1", "rerewrw1"));
		persons.add(new Person("erwrw2", "rewrw2", "rerewrw2"));
		persons.add(new Person("erwrw3", "rewrw3", "rerewrw3"));
		persons.add(new Person("erwrw4", "rewrw4", "rerewrw4"));
		persons.add(new Person("erwrw5", "rewrw5", "rerewrw5"));

		persons.add(personOfInterest);

		System.out.println(persons.indexOf(personOfInterest));
		System.out.println(persons
				.indexOf(new Person("first", "second", "last")));
	}
}