package org.elsysbg.courses.ip.lists;

import java.util.ArrayList;

public class ListWithStrings {

	public static void main(String[] args) {
		String[] arguments = { "1", "2", "3", "4" };
		ArrayList<String> argList = new ArrayList<String>();
		for (String arg : arguments) {
			argList.add(arg);
		}

		System.out.println(argList.indexOf(arguments[3]));
		System.out.println(argList.indexOf("4"));
	}
}