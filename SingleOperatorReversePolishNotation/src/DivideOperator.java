public class DivideOperator extends BaseOperator {

	public DivideOperator(double[] operands) {
		super(operands);
	}

	@Override
	public double execute() {
		double result = operands[0];
		for (int i = 1; i < operands.length; i++) {
			result /= operands[i];
		}

		return result;
	}

}