public abstract class BaseOperator implements Operator {

	protected double[] operands;

	public BaseOperator(double[] operands) {
		this.operands = operands;
	}

}
