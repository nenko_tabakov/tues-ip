public class MultiplyOperator extends BaseOperator {

	public MultiplyOperator(double[] operands) {
		super(operands);
	}

	@Override
	public double execute() {
		double result = 1;
		for (double operand : operands) {
			result *= operand;
		}

		return result;
	}

}
