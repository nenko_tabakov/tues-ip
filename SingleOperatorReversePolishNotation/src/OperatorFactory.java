public class OperatorFactory {

	public static Operator create(String operator, double[] operands) {
		if ("+".equals(operator)) {
			return new PlusOperator(operands);
		}

		if ("-".equals(operator)) {
			return new MinusOperator(operands);
		}

		if ("*".equals(operator)) {
			return new MultiplyOperator(operands);
		}

		if ("/".equals(operator)) {
			return new DivideOperator(operands);
		}

		throw new IllegalArgumentException("Unknown operator!");
	}
}
