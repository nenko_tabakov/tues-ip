public class Application {

	public static void main(String[] args) {
		int lastIndex = args.length - 1;
		String operator = args[lastIndex];

		double[] operands = new double[lastIndex];
		for (int i = 0; i < lastIndex; i++) {
			operands[i] = Double.parseDouble(args[i]);
		}
		Operator operatorToExecute = OperatorFactory.create(operator, operands);
		System.out.println(operatorToExecute.execute());

	}
}
