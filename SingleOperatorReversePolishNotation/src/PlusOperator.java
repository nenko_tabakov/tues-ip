public class PlusOperator extends BaseOperator {

	public PlusOperator(double[] operands) {
		super(operands);
	}

	@Override
	public double execute() {
		double result = 0;
		for (double operand : operands) {
			result += operand;
		}

		return result;
	}
}
