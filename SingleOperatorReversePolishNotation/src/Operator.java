
public interface Operator {

	double execute();
}
